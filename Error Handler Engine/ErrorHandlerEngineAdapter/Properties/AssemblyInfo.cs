﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ErrorHandlerEngine")]
[assembly: AssemblyDescription("Error Control System: 1.ErrorHandlerEngine 2.ErrorLogAnalyzer 3.BugTracker  \n\rCreator Info:\n\r Mr.Behzad Khosravifar [mailto:Behzad.Khosravifar@gmail.com] \n\r Mr.Ayuob Amini  [mailto:Ayyub.Amini@gmail.com]")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Shoniz")]
[assembly: AssemblyProduct("ErrorHandlerEngine")]
[assembly: AssemblyCopyright("Copyright © 2014-2015 Shoniz Corporation.")]
[assembly: AssemblyTrademark("Shoniz")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f57a1667-aaf4-4ac7-8941-c3394472e155")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
